<%@ page import="model.vo.Beer" %>
<%@ page import="model.dao.impl.DBBeerDAO" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%  long id = Long.parseLong((String) request.getAttribute("id"));
    Beer tmp = new DBBeerDAO().getBeerById(id); %>
<html>
<head>
    <meta charset="utf-8" />
    <title><%=tmp.getName()%></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="jsp/css/style.css" rel="stylesheet">
</head>
<body>

</body>
</html>
