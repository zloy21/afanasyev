<%@ page import="controller.CheckCookie" %>
<%@ page import="model.vo.Profile" %>
<%@ page import="model.dao.impl.DBProfileDAO" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My profile</title>
    <meta name= "keywords" content="" />
    <meta name="description" content="" />
    <link href="/jsp/css/style.css" rel="stylesheet">
</head>
<body>
<% String userName = CheckCookie.current_user(request);
 if(userName == null) response.sendRedirect("/login");
 Profile currentProfile = new DBProfileDAO().getProfileByLogin(userName); %>

<div class="wrapper">

    <jsp:include page="/jsp/block/header.jsp"/>
    <main class="content">


        <div class="kabinet">
            <h1> Личный кабинет </h1>
            <p>Логин :  <%=currentProfile.getLogin()%> </p>
            <p>Имя : <%=currentProfile.getName()%> </p>
            <p>Почта :  <%=currentProfile.getEmail()%> </p>

            <h2>Список магазинов</h2>
            <table border="1" cellpadding="7" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td colspan="2" bgcolor="#8ebff0" align="center">Магазины</td>
                </tr>
                <tr><td width="50%" valign="top" align="center">Название магазина</td><td width="50%" valign="top" align="center">Адрес магазина</td>
                </tr>
                </tbody>
            </table>

            <a href="/myprofile/edit" class="edit-prof">редактировать профиль</a>
            <a href="/myprofile/shops" class="add-magaz">добавить/удалить магазин</a>

        </div>





    </main>


</div>

<jsp:include page="/jsp/block/footer.jsp"/>

</body>
</html>
