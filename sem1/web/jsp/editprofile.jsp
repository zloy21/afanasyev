
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8" />
    <title>Edit profile</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="/jsp/css/style.css" rel="stylesheet">
</head>
<body>

<div class="wrapper">

    <jsp:include page="/jsp/block/header.jsp"/>

    <main class="content">
        <br/>
        <br/>

        Fill in the fields that you want to change and Old Password
        <form action="/editprofile" method="post">
            <b>Email: </b> <br/> <input type="text" name="email" /> <br/>
            <b>Name: </b> <br/> <input type="text" name="name" /> <br/>
            <b>Password: </b> <br/> <input type="password" name="password" /> <br/>
            <b>Retry password pls:</b> <br/> <input type="password" name="password2" /><br/>
            <b>Old password :</b> <br/> <input type="password" name="old_password" /> <br/> <br/>
            <input type="submit" value="Edit" />


        </form>

    </main>

</div>

<jsp:include page="/jsp/block/footer.jsp"/>

</body>
</html>
