<%@ page import="controller.CheckCookie" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% String userName = CheckCookie.current_user(request); %>
<header class="header">
    <ul class="cssmenu">

        <li><a href="/main">Main</a></li>

        <li><a href="/beers">Beers</a></li>

        <li><a href="/checkout">Checkout</a></li>

        <li><a href="/manufacturer">Manufacturer</a></li>

        <% if(userName != null) { %>

        <li><a href="/myprofile">My Profile</a></li>

        <li><a href="/logout">Logout</a></li>

        <% } else { %>

        <li><a href="/login">Login</a></li>

        <li><a href="/reg">Reg</a></li>

        <% } %>
    </ul>
</header>
