<%@ page import="model.vo.Beer" %>
<%@ page import="java.util.List" %>
<%@ page import="model.dao.impl.DBBeerDAO" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% List<Beer> beers = new DBBeerDAO().getAllBeers();%>
<html>
<head>
    <title>Beer list</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="/jsp/css/style.css" rel="stylesheet">
</head>
<body>

<div class="wrapper">

    <jsp:include page="/jsp/block/header.jsp"/>
    <main class="content">

        <div class="beer-list">

            <h1>Beer list</h1>
            <hr />

            <table border="1" cellpadding="7" cellspacing="0" width="100%">
                <tbody><tr><td colspan="3" bgcolor="#8ebff0" align="center"></td>
                </tr>

                <tr><td valign="top" width="60%" align="center">Название</td><td valign="top" width="20%">Алкоголь %</td> <td width="20%">Цена 1 кег </td></tr>

                <% for(Beer beer : beers) { %>

                <tr><td><a href="./beer?id=<%=beer.getId()%>"> <%=beer.getName()%> </a> </td> <td><%=beer.getAlcho()%></td> <td><%=beer.getPrice()%></td></tr>
                <% } %>
                </tbody>
            </table>
        </div>
    </main>


</div><!-- .wrapper -->

<jsp:include page="/jsp/block/footer.jsp"/>


</body>
</html>
