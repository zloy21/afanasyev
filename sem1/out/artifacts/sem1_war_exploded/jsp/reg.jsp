
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8" />
    <title>Registration</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="/jsp/css/style.css" rel="stylesheet">
</head>
<body>
<% String[] err = {
        request.getParameter("l"),
        request.getParameter("e"),
        request.getParameter("n"),
        request.getParameter("p")
};
    %>

<div class="wrapper">

    <jsp:include page="/jsp/block/header.jsp"/>

    <main class="content">
        <br/>
        <br/>

        <form action="/reg" method="post">
            All fields must be filled  <br/>
            <b>Login: </b> <br/> <input type="text" name="login" /> <%if (err[0] != null && err[0].equals("1")) { %> Login invalid <% } %>  <br/>
            Login length from 5 to 11 characters are allowed only English characters , the login is case sensitive <br/>
            <b>Email: </b> <br/> <input type="text" name="email" /> <%if (err[1] !=null && err[1].equals("1")) { %> Email invalid <% } %> <br/>
            The maximum length of 50 characters email to register is available only @ gmail.com <br/>
            <b>Name: </b> <br/> <input type="text" name="name" /> <br/> <% if (err[2] != null && err[2].equals("1")) { %> Name invalid <% } %>
            The maximum length of 50 characters <br/>
            <b>Password: </b> <br/> <input type="password" name="pass" /> <% if(err[3] != null && err[3].equals("1")) { %> Pass invalid <% } %> <br/>
            Password length from 6 to 12 characters , the passwords must match <br/>
            <b>Retry password pls</b> <br/> <input type="password" name="pass2" /> <br/> <br/>
            <input type="submit" value="Registration" />


        </form>

    </main>

</div>

<jsp:include page="/jsp/block/footer.jsp"/>

</body>
</html>
