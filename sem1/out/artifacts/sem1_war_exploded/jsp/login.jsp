
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8" />
    <title>Login</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="jsp/css/style.css" rel="stylesheet">
</head>
<body>
<% String logErr = request.getParameter("user");
   String passErr = request.getParameter("pass");
%>

<div class="wrapper">

    <jsp:include page="/jsp/block/header.jsp"/>

    <main class="content">
        <br/>
        <br/>

        <form action="/log" method="post">

            <b>Login: </b> <br/> <input type="text" name="login" <% if(passErr != null && passErr.equals("no")) { %>  value="<%=logErr%>" <% } %> /> <br/>
            <% if(logErr != null && logErr.equals("no")) { %>
            Not user with login
            <br/>
            <% } %>
            <b>Password: </b> <br/> <input type="password" name="pass" /> <br/>
            <% if(passErr != null && passErr.equals("no")) { %>
            Invalid password
            <br/>
            <% } %>
            <input type="submit" value="Login" />


        </form>

    </main>

</div>

<jsp:include page="/jsp/block/footer.jsp"/>

</body>
</html>

