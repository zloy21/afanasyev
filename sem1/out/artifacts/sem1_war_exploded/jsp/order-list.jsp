<%@ page import="model.vo.Beer" %>
<%@ page import="java.util.List" %>
<%@ page import="model.dao.impl.DBBeerDAO" %>
<%@ page import="model.vo.Shop" %>
<%@ page import="model.dao.impl.DBShopDAO" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="/jsp/css/style.css" rel="stylesheet">
</head>
<body>

<div class="wrapper">

    <jsp:include page="/jsp/block/header.jsp"/>
    <main class="content">
    <%List<Beer> beers = new DBBeerDAO().getAllBeers();
      List<Shop> shops = new DBShopDAO().getShopsByProfileId(1);%>


        <form name="beer-order" method="post" action="/beer-order">


            <p>Select beer type:</p>
            <p>
                <select name="type" size="1">
                    <% for(Beer beer : beers) { %>
                    <option name="<%=beer.getId()%>"> <%=beer.getName()%> </option>
                    <% } %>
                </select>
            </p>

            <p>
                Введите количество кег:<br>
                <input type="text" name="summ" >
            </p>

            <p>Адрес доставки:</p>
            <p>
                <select name="address" size="1">
                    <% for(Shop shop : shops) { %>
                    <option name="<%=shop.getId()%>"> <%=shop.getAddress()%> </option>
                    <% } %>
                </select>
            </p>



            <p>
                <input type="submit" value="Сделать заказ" width="20">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="reset" value="Отмена ">
            </p>
        </form>





    </main>


</div>

<jsp:include page="/jsp/block/footer.jsp"/>

</body>
</html>
