<%@ page import="model.vo.Beer" %>
<%@ page import="model.dao.impl.DBBeerDAO" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% Beer tmp = new DBBeerDAO().getBeerById(Long.parseLong(request.getParameter("id"))); %>
<html>
<head>
    <meta charset="utf-8" />
    <title><%=tmp.getName()%></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="jsp/css/style.css" rel="stylesheet">
</head>
<body>

<div class="wrapper">

    <jsp:include page="/jsp/block/header.jsp"/>

        <main class="content">

            <div class="beer-info">
                <div class="beer-info-elem">
                    <h1><%=tmp.getName()%></h1>
                    <hr/>
                    <div class="beer-info-content">
                        <b>Price : </b> <%=tmp.getPrice()%> <br/>
                        <b>Description : </b> <%=tmp.getDescription()%> <br/>
                        <b>Count : </b> <%=tmp.getCount()%> <br/>
                        <b>Alcohol : </b> <%=tmp.getAlcho()%>
                    </div>
                </div>

            </div>
        </main>

</div>

<jsp:include page="/jsp/block/footer.jsp"/>

</body>
</html>
