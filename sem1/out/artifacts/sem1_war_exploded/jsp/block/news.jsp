<%@ page import="model.vo.New" %>
<%@ page import="java.util.List" %>
<%@ page import="model.dao.impl.DBNewDAO" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% DBNewDAO db = new DBNewDAO();
    List<New> news = db.getNews(3);
   String imgHref;
%>
<main class="content">

    <div class="news">
        <% for(New tmpNew : news) {
           imgHref = tmpNew.getImg();
        %>
        <div class="news-elem">
            <h1><%=tmpNew.getTitle()%></h1>
            <hr/>
        <% if(imgHref!= null && !imgHref.isEmpty()) { %>
            <img src="<%=tmpNew.getImg()%>"/>
        <% } %>
            <div class="news-content"><%=tmpNew.getText()%></div>
        </div>
        <% } %>

    </div>
</main>