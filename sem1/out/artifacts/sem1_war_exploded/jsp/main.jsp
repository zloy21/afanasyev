<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Beer King - Main page</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="/jsp/css/style.css" rel="stylesheet">
</head>
<body>

<div class="wrapper">

    <jsp:include page="/jsp/block/header.jsp"/>
    <jsp:include page="/jsp/block/news.jsp"/>


</div>

<jsp:include page="/jsp/block/footer.jsp"/>

</body>
</html>
