package model.vo;

public class Trade {

    private long id;
    private long profile_id;
    private long shop_id;
    private long beer_id;
    private int count;
    private String statuc;
    private double cost;

    public Trade(long id, long profile_id, long shop_id, long beer_id, int count, String statuc, double cost) {
        this.id = id;
        this.profile_id = profile_id;
        this.shop_id = shop_id;
        this.beer_id = beer_id;
        this.count = count;
        this.statuc = statuc;
        this.cost = cost;
    }

    public Trade(long profile_id, long shop_id, long beer_id, int count, String statuc, double cost) {
        this.profile_id = profile_id;
        this.shop_id = shop_id;
        this.beer_id = beer_id;
        this.count = count;
        this.statuc = statuc;
        this.cost = cost;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(long profile_id) {
        this.profile_id = profile_id;
    }

    public long getShop_id() {
        return shop_id;
    }

    public void setShop_id(long shop_id) {
        this.shop_id = shop_id;
    }

    public long getBeer_id() {
        return beer_id;
    }

    public void setBeer_id(long beer_id) {
        this.beer_id = beer_id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getStatuc() {
        return statuc;
    }

    public void setStatuc(String statuc) {
        this.statuc = statuc;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
