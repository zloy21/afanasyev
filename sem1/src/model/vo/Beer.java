package model.vo;


public class Beer {
    private long id;
    private String name;
    private double price;
    private int count;
    private String description;
    private double alcho;

    public Beer(String name, double price, int count, String description, double alcho) {
        this.name = name;
        this.price = price;
        this.count = count;
        this.description = description;
        this.alcho = alcho;
    }

    public Beer(long id, String name, double price, int count, String description, double alcho) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.count = count;
        this.description = description;
        this.alcho = alcho;
    }

    public String getName() {
        return name;
    }

    public double getAlcho() {
        return alcho;
    }

    public void setAlcho(double alcho) {
        this.alcho = alcho;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
