package model.vo;


public class Profile {

    private long id;
    private String login;
    private String pass;
    private String email;
    private String name;

    public Profile(long id, String login, String pass, String email, String name) {
        this.id = id;
        this.login = login;
        this.pass = pass;
        this.email = email;
        this.name = name;
    }

    public Profile(String login, String pass, String email, String name) {
        this.login = login;
        this.pass = pass;
        this.email = email;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
