package model.dao;

import model.vo.New;

import java.sql.SQLException;
import java.util.List;


public interface NewDAO {

    public List<New> getAllNews() throws SQLException;

    public List<New> getNews(long countNews) throws SQLException;

    public long getCountNews() throws SQLException;

    public New getNewById(long id) throws SQLException;

    public void creteNew(New tmp) throws SQLException;

    public void updateNew(New tmp) throws SQLException;

    public void deleteNewByID(long id) throws SQLException;



}
