package model.dao;

import model.vo.Beer;

import java.sql.SQLException;
import java.util.List;


public interface BeerDAO {

    public List<Beer> getAllBeers() throws SQLException;

    public Beer getBeerById(long id) throws SQLException;

    public void deleteBeerById(long id) throws SQLException;

    public void updateBeer(Beer tmp) throws SQLException;

    public void createBeer(Beer tmp) throws SQLException;

}
