package model.dao.impl;

import model.dao.TradeDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBTradeDAO implements TradeDAO {

    Connection conn;

    public DBTradeDAO() {
        try {
            Class.forName(Constants.DBDriver);
            conn = DriverManager.getConnection(Constants.DBHost, Constants.DBLogin, Constants.DBPass);
            createTable();
        } catch (SQLException e) {
            System.err.println("я не хочу подключаться к бд");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createTable() {

    }

}
