package model.dao.impl;

import model.dao.ShopDAO;
import model.vo.Shop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;


public class DBShopDAO implements ShopDAO {
    Connection conn;

    public DBShopDAO() {
        try {
            Class.forName(Constants.DBDriver);
            conn = DriverManager.getConnection(Constants.DBHost, Constants.DBLogin, Constants.DBPass);
            createTable();
        } catch (SQLException e) {
            System.err.println("я не хочу подключаться к бд");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Shop> getShopsByProfileId(long id) throws SQLException {
        return null;
    }

    @Override
    public List<Shop> getAllShops() throws SQLException {
        return null;
    }

    @Override
    public Shop getShopByID() throws SQLException {
        return null;
    }

    @Override
    public void createShop(Shop tmpShop) throws SQLException {

    }

    @Override
    public void deleteShop(Shop tmpShop) throws SQLException {

    }

    @Override
    public void updateShop(Shop tmpShop) throws SQLException {

    }

    private void createTable() {

    }
}
