package model.dao.impl;


public interface Constants {
    public String DBDriver = "org.postgresql.Driver";
    public String DBHost = "jdbc:postgresql://127.0.0.1:5432/beer";
    public String DBLogin = "postgres";
    public String DBPass = "postgres";
}
