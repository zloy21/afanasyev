package model.dao.impl;

import model.dao.ProfileDAO;
import model.vo.Profile;
import model.vo.Shop;

import java.sql.*;


public class DBProfileDAO implements ProfileDAO {

    Connection conn;

    public DBProfileDAO() {
        try {
            Class.forName(Constants.DBDriver);
            conn = DriverManager.getConnection(Constants.DBHost, Constants.DBLogin, Constants.DBPass);
            createTable();
        } catch (SQLException e) {
            System.err.println("я не хочу подключаться к бд");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createProfile(Profile tmp) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("insert into profiles(pass, email, login, name) values (?,?,?,?)");
        preparedStatement.setString(1, tmp.getPass());
        preparedStatement.setString(2, tmp.getEmail());
        preparedStatement.setString(3, tmp.getLogin());
        preparedStatement.setString(4, tmp.getName());
        preparedStatement.executeUpdate();
    }

    @Override
    public String getPassProfileByLogin(String login) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("select pass from profiles where profiles.login=?");
        preparedStatement.setString(1, login);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next()) {
            return resultSet.getString("pass");
        }
        return null;
    }

    @Override
    public Profile getProfileByLogin(String login) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("select * from profiles where profiles.login=?");
        preparedStatement.setString(1, login);
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next()) {
            Long id = resultSet.getLong("id");
            String pass = resultSet.getString("pass");
            String email = resultSet.getString("email");
            String name = resultSet.getString("name");
            return new Profile(id, login, pass, email, name);
        }
        return null;
    }

    @Override
    public void addShop(Shop shop) throws SQLException {

    }

    @Override
    public void editProfile(Profile profile) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("update profiles set pass=? ,email=?, name=? where profiles.id=?");
        preparedStatement.setString(1, profile.getPass());
        preparedStatement.setString(2, profile.getEmail());
        preparedStatement.setString(3, profile.getName());
        preparedStatement.setLong(4, profile.getId());
        preparedStatement.executeUpdate();
    }

    private void createTable() {

    }
}
