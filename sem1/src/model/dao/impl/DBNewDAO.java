package model.dao.impl;

import model.dao.NewDAO;
import model.vo.New;

import java.sql.*;
import java.sql.Date;
import java.util.*;


public class DBNewDAO implements NewDAO {

    Connection conn;

    public DBNewDAO() {
        try {
            Class.forName(Constants.DBDriver);
            conn = DriverManager.getConnection(Constants.DBHost, Constants.DBLogin, Constants.DBPass);
            createTable();
        } catch (SQLException e) {
            System.err.println("я не хочу подключаться к бд");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<New> getAllNews() throws SQLException {
        List<New> array = new ArrayList<New>();
        PreparedStatement preparedStatement = conn.prepareStatement("select * from news order by news.date desc");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            long id = resultSet.getLong("id");
            String title = resultSet.getString("title");
            String text = resultSet.getString("text");
            Date date = resultSet.getDate("date");
            String img = resultSet.getString("img");
            New tmp = new New(id, title, text, date);
            if (img != null && !img.isEmpty()) {
                tmp.setImg(img);
            }
            array.add(tmp);
        }

        return array;
    }

    @Override
    public List<New> getNews(long countNews) throws SQLException {
        List<New> array = new ArrayList<New>();
            PreparedStatement preparedStatement = conn.prepareStatement("select * from news order by news.date desc");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next() && array.size() < countNews) {
                long id = resultSet.getLong("id");
                String title = resultSet.getString("title");
                String text = resultSet.getString("text");
                Date date = resultSet.getDate("date");
                String img = resultSet.getString("img");
                New tmp = new New(id, title, text, date);
                if (img != null && !img.isEmpty()) {
                    tmp.setImg(img);
                }
//                System.out.println("NEWS ADD. TITLE : " + title);
                array.add(tmp);
            }

        return array;
    }

    @Override
    public long getCountNews() throws SQLException {
        return 0;
    }

    @Override
    public New getNewById(long id) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("select * from news where news.id=?");
        preparedStatement.setLong(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            String title = resultSet.getString("title");
            String text = resultSet.getString("text");
            Date date = resultSet.getDate("date");
            String img = resultSet.getString("img");
            New tmp = new New(id, title, text, date);
            if (img != null && !img.isEmpty()) {
                tmp.setImg(img);
            }
//            System.out.println("NEWS ADD. TITLE : " + title);
            return tmp;
        }

        return null;
    }

    @Override
    public void creteNew(New tmp) throws SQLException {
        String title = tmp.getTitle();
        String text = tmp.getText();
        java.util.Date date = tmp.getDate();
        String img = tmp.getImg();

        PreparedStatement preparedStatement = conn.prepareStatement("insert into news (title, text, date, img) values (?,?,?,?)");
        preparedStatement.setString(1, title);
        preparedStatement.setString(2, text);
        preparedStatement.setDate(3, (Date) date);
        preparedStatement.setString(4, img);
        preparedStatement.executeUpdate();

    }

    @Override
    public void updateNew(New tmp) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("update news set title=?, text=?, date=?, img=? where news.id=?");
        preparedStatement.setString(1, tmp.getTitle());
        preparedStatement.setString(2, tmp.getText());
        preparedStatement.setDate(3, (Date) tmp.getDate());
        preparedStatement.setString(4, tmp.getImg());
        preparedStatement.setLong(5, tmp.getId());
        preparedStatement.executeUpdate();
    }

    @Override
    public void deleteNewByID(long id) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("delete from news where news.id=?");
        preparedStatement.setLong(1, id);
        preparedStatement.executeUpdate();
    }

    private void createTable() {

    }
}
