package model.dao.impl;

import model.dao.BeerDAO;
import model.vo.Beer;
import model.vo.New;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBBeerDAO implements BeerDAO {
    Connection conn;

    public DBBeerDAO() {
        try {
            Class.forName(Constants.DBDriver);
            conn = DriverManager.getConnection(Constants.DBHost, Constants.DBLogin, Constants.DBPass);
            createTable();
        } catch (SQLException e) {
            System.err.println("я не хочу подключаться к бд");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Beer> getAllBeers() throws SQLException {
        List<Beer> array = new ArrayList<Beer>();
        PreparedStatement preparedStatement = conn.prepareStatement("select * from beers ");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            double price = resultSet.getDouble("price");
            int count = resultSet.getInt("count");
            String desc = resultSet.getString("description");
            double alcho = resultSet.getDouble("alcho");
            Beer tmp = new Beer(id, name, price, count, desc, alcho);
            array.add(tmp);
        }

        return array;
    }

    @Override
    public Beer getBeerById(long id) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("select * from beers where beers.id=?");
        preparedStatement.setLong(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            String name = resultSet.getString("name");
            double price = resultSet.getDouble("price");
            int count = resultSet.getInt("count");
            String desc = resultSet.getString("description");
            double alcho = resultSet.getDouble("alcho");
            Beer tmp = new Beer(id, name, price, count, desc, alcho);
            return tmp;
        }

        return null;
    }

    @Override
    public void deleteBeerById(long id) throws SQLException {
        PreparedStatement preparedStatement = conn.prepareStatement("delete from beers where beers.id=?");
        preparedStatement.setLong(1, id);
        preparedStatement.executeUpdate();
    }

    @Override
    public void updateBeer(Beer tmp) throws SQLException {

    }

    @Override
    public void createBeer(Beer tmp) throws SQLException {

    }

    private void createTable() {

    }
}
