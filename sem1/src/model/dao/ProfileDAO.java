package model.dao;

import model.vo.Profile;
import model.vo.Shop;

import java.sql.SQLException;


public interface ProfileDAO {

    public void createProfile(Profile tmp) throws SQLException;

    public String getPassProfileByLogin(String login) throws SQLException;

    public Profile getProfileByLogin(String login) throws SQLException;

    public void addShop(Shop shop) throws SQLException;

    public void editProfile(Profile profile) throws SQLException;

}
