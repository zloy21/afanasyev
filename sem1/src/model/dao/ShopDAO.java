package model.dao;

import model.vo.Shop;

import java.sql.SQLException;
import java.util.List;


public interface ShopDAO {

    public List<Shop> getShopsByProfileId(long id) throws SQLException;

    public List<Shop> getAllShops() throws SQLException;

    public Shop getShopByID() throws SQLException;

    public void createShop(Shop tmpShop) throws SQLException;

    public void deleteShop(Shop tmpShop) throws SQLException;

    public void updateShop(Shop tmpShop) throws SQLException;
}
