package controller;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class Logout extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (CheckCookie.current_user(request) != null) {
            HttpSession hs = request.getSession();
            Cookie cookie = CheckCookie.getUserCookie(request);
            cookie.setMaxAge(0);
            response.addCookie(cookie);
            hs.removeAttribute("current_user");
        }
        response.sendRedirect("/main");
    }

}
