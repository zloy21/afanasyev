package controller;
import model.dao.impl.DBProfileDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public class Login extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String pass = request.getParameter("pass");
        log(request, response, login, pass);
    }

    private String md5(String tmpLine) {
        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(tmpLine.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);
        while ( md5Hex.length() < 32 ) {
            md5Hex = "0" + md5Hex;
        }

        return md5Hex;
    }

    public void log(HttpServletRequest request, HttpServletResponse response, String login, String pass) throws IOException {
        if(login.isEmpty() || login == null) {
            response.sendRedirect("/login");
            return;
        }

        String truePass = null;
        try {
            truePass = new DBProfileDAO().getPassProfileByLogin(login);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(truePass == null) {
            response.sendRedirect("/login?user=no");
            return;
        } else {

            if (md5(pass).equals(truePass)) {
                Cookie cookie = new Cookie("user", login);
                cookie.setMaxAge(3600);
                cookie.setPath("/");
                response.addCookie(cookie);
                HttpSession hs = request.getSession();
                hs.setAttribute("current_user", login);
                response.sendRedirect("/main");
            } else {

                response.sendRedirect("/login?pass=no&user=" + login);
            }
        }
    }

}
