package controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CheckCookie {

    public static String current_user(HttpServletRequest req) {
        HttpSession hs = req.getSession();
        return (String) hs.getAttribute("current_user");
    }

    public static Cookie getUserCookie(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("user")) {
                return cookie;
            }
        }
        return null;
    }

}
