package controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Helpers {

    public static String md5(String tmpLine) {
        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(tmpLine.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);
        while ( md5Hex.length() < 32 ) {
            md5Hex = "0" + md5Hex;
        }

        return md5Hex;
    }

    public static String current_user(HttpServletRequest req) {
        HttpSession hs = req.getSession();
        return (String) hs.getAttribute("current_user");
    }

    public static Cookie getUserCookie(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("user")) {
                return cookie;
            }
        }
        return null;
    }

}
