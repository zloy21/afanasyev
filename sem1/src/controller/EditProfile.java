package controller;

import model.dao.impl.DBProfileDAO;
import model.vo.Profile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class EditProfile extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("edit profile!!!");
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String oldPassword = request.getParameter("old_password");
        String userName = Helpers.current_user(request);
        Profile currentProfile = null;
        try {
            currentProfile = new DBProfileDAO().getProfileByLogin(userName);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(Helpers.md5(oldPassword).equals(currentProfile.getPass())) {
            if(email != null && !email.isEmpty()) currentProfile.setEmail(email);
            if(name != null && !name.isEmpty()) currentProfile.setName(name);
            if(password.equals(password2) && password!= null && !password.isEmpty()) currentProfile.setPass(Helpers.md5(password));
            try {
                new DBProfileDAO().editProfile(currentProfile);
                response.sendRedirect("/myprofile");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
