package controller;

import model.dao.impl.DBProfileDAO;
import model.vo.Profile;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public class Registration extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        String pass = request.getParameter("pass");
        String pass2 = request.getParameter("pass2");
        byte[] err = new byte[4];
        if(login.length() < 5 || login.length() > 11 || login.matches("[a-zA-z]+")) {
            err[0] = 1;
        }
        if(!email.matches("[A-Za-z_//.-]+@gmail.com") || email.length()>50 || email.length()==0) {
            err[1] = 1;
        }

        if(name.length()==0 || name.length()>50) {
            err[2] = 1;
        }

        if(!pass.equals(pass2) || pass.length() < 6 || pass.length() > 12) {
            err[3] = 1;
        }
        for(int i = 0; i < err.length; i++) {
            if(err[i] != 0) {
                response.sendRedirect("/reg?l=" + err[0] + "&e=" + err[1] + "&n=" + err[2] + "&p=" + err[3]);
                return;
            }
        }

        Profile tmp = new Profile(login, Helpers.md5(pass), email, name);
        try {
            new DBProfileDAO().createProfile(tmp);
            new Login().log(request, response, login, pass);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //отображение ошибок из атрибутов
        include(request,response,"/jsp/reg.jsp");
    }

    private void include(HttpServletRequest request, HttpServletResponse response, String jspPath) throws ServletException,
            IOException, NullPointerException {
        request.setCharacterEncoding("UTF-8");
        RequestDispatcher rd = request.getRequestDispatcher(jspPath);
        rd.include(request, response);
    }

}
